package main

import (
	lib "gitlab.com/thoxy/psbdmp-bg/libs"
	"gitlab.com/thoxy/psbdmp-bg/psbdmp"
)

func init() {
	lib.HomeMessage()
}

func main() {

	lib.HomeMessage()
	psbdmp.Menu()
	lib.Pause(true)

}
