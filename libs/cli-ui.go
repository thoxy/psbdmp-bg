package libs

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	au "github.com/logrusorgru/aurora"
)

//Print Home message
func HomeMessage() {

	if runtime.GOOS == "windows" {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run() //Clear Cmd on Window
	} else {
		c := exec.Command("clear")
		c.Stdout = os.Stdout
		c.Run() //Clear terminal in Mac OSX, Linux and others
	}

	fmt.Println(au.Bold(au.Red("\n  ██████╗ ███████╗██████╗ ██████╗ ███╗   ███╗██████╗       ██████╗  ██████╗ ")))
	fmt.Println(au.Bold(au.Brown("  ██╔══██╗██╔════╝██╔══██╗██╔══██╗████╗ ████║██╔══██╗      ██╔══██╗██╔════╝ ")))
	fmt.Println(au.Bold(au.Green("  ██████╔╝███████╗██████╔╝██║  ██║██╔████╔██║██████╔╝█████╗██████╔╝██║  ███╗")))
	fmt.Println(au.Bold(au.Blue("  ██╔═══╝ ╚════██║██╔══██╗██║  ██║██║╚██╔╝██║██╔═══╝ ╚════╝██╔══██╗██║   ██║")))
	fmt.Println(au.Bold(au.Magenta("  ██║     ███████║██████╔╝██████╔╝██║ ╚═╝ ██║██║           ██████╔╝╚██████╔╝")))
	fmt.Println(au.Bold(au.Black("  ╚═╝     ╚══════╝╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝           ╚═════╝  ╚═════╝")))
	fmt.Println(au.Bold(au.Black(" ╔══════════════════════════════════════════════════════════════════════════╗")))
	fmt.Println(au.Bold(au.Black(" ║")), au.Bold("                       Developed with "), au.Bold(au.Red("♥")), au.Bold(" by "), au.Bold(au.Cyan("Thoxy                    ")), au.Bold(au.Black(" ║")))
	fmt.Println(au.Bold(au.Black(" ╚══════════════════════════════════════════════════════════════════════════╝\n")))
}

//Waiting 3 sec colored
func Wait3sec() {
	fmt.Println(au.BgRed(au.Bold(" Wait 3 second! ")))
	time.Sleep(time.Second * 1)
	fmt.Printf("\033[2K")
	fmt.Printf("\033[A")
	fmt.Println(au.Bold(au.Bold(au.BgBrown(" Wait 2 second! "))))
	time.Sleep(time.Second * 1)
	fmt.Printf("\033[2K")
	fmt.Printf("\033[A")
	fmt.Println(au.Black(au.BgGray(au.Bold(" Wait 1 second! "))))
	time.Sleep(time.Second * 1)
	fmt.Printf("\033[2K")
	fmt.Printf("\033[A")
	fmt.Println(au.BgGreen(au.Bold(" Lets Go!       \n")))
	time.Sleep(time.Second * 1)
}

//Choice Yes or No
func Ask4confirm(str string) bool {
	var s string
	fmt.Print(" ", au.Bold(str), " (", au.Green(au.Bold("y")), "/", au.Red(au.Bold("N")), "): ")
	_, err := fmt.Scan(&s)
	if err != nil {
		panic(err)
	}
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	if s == "y" || s == "Y" || s == "yes" || s == "YES" || s == "Yes" {
		return true
	} else {
		return false
	}

}

//Do pause and waiting confirmation
func Pause(exit bool) {
	if exit == true {
		fmt.Print(au.Bold("\nPress 'Return' key to exit... "))
		bufio.NewReader(os.Stdin).ReadBytes('\n')
		os.Exit(0)
	} else {
		fmt.Print(au.Bold("\nPress 'Return' key to continue... "))
		bufio.NewReader(os.Stdin).ReadBytes('\n')
	}
}
