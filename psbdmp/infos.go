package psbdmp

import (
	"regexp"
	"strconv"
	"sync"
)

//pages infos
func Infos() *MaxP {
	p := MaxP{}
	var s1 string
	var s2 string

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		s1 = grabstr("http://psbdmp.ws/dumps", 15)
		s2 = grabstr("http://psbdmp.ws/daily", 15)
		defer wg.Done()
	}()
	wg.Wait()

	var r *regexp.Regexp
	wg.Add(2)
	go func() {
		//Grab total count of pastes
		r, _ = regexp.Compile("Total\\scount:\\s<span\\sclass=\"label\\slabel-default\\slabel-as-badge\">\\s([\\d]{1,})</span>")
		for _, t := range r.FindAllStringSubmatch(s1, -1) {
			ii, _ := strconv.Atoi(t[1])
			p.pastes = ii
		}
		//Grab number of dumps pages
		r, _ = regexp.Compile("<a\\shref=\"http://psbdmp.ws/dumps/[\\d]{0,}\"\\sdata-ci-pagination-page=\"([\\d]{0,})\">Last")
		for _, t := range r.FindAllStringSubmatch(s1, -1) {
			ii, _ := strconv.Atoi(t[1])
			p.pages = ii
		}
		defer wg.Done()
	}()
	go func() {
		//Grab last daily pages
		r, _ = regexp.Compile("<tr><td><a\\shref='/daily/([\\d]{0,})'>([\\d]{0,4}-[\\d]{0,2}-[\\d]{0,2})<")
		for i, t := range r.FindAllStringSubmatch(s2, -1) {
			if i < 1 {
				ii, _ := strconv.Atoi(t[1])
				p.days = ii
				p.date = t[2]
			}
		}
		defer wg.Done()
	}()
	wg.Wait()
	return &p
}
