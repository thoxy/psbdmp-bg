package psbdmp

import (
	"fmt"
	"strconv"
	"time"

	au "github.com/logrusorgru/aurora"

	l "gitlab.com/thoxy/psbdmp-bg/libs"
)

func (c Config) LaunchG(link []string, i MaxP) {

	l.Wait3sec()
	fmt.Println(au.Bold("\n══════════════════════════════════════════════════════════════════════════════"))
	fmt.Println(au.BgMagenta(au.Bold(" Grabber : ")))
	fmt.Println("══════════════════════════════════════════════════════════════════════════════")
	fmt.Println(au.Bold(" Regex Used : "), c.regex)
	fmt.Println(au.Bold(" Total test link count : "), len(link), "\n")

	str := c.StartWork()
	str2 := l.RemoveDuplicatesUnordered(str)

	fmt.Println(au.Bold("\n══════════════════════════════════════════════════════════════════════════════"))
	fmt.Println(au.BgGreen(au.Bold(" Result : ")))
	fmt.Println(au.Bold(" Grabbed Count  : "), au.Brown(len(str)))
	fmt.Println(au.Bold(" Dupe Count     : "), au.Red(len(str)-len(str2)))
	fmt.Println(au.Bold(" Total no Dupe  : "), au.Bold(au.Green(len(str2))))
	var Name string
	if len(str) != 0 {
		t := time.Now()
		Name = "Grabbed_" + t.Format("2006-01-02_15,04,05") + ".txt"
		fmt.Println(au.Bold(" Saved list     : "), au.Cyan(Name))
	}
	fmt.Println(au.Bold("══════════════════════════════════════════════════════════════════════════════"))

	if len(str) != 0 {
		fmt.Println(au.BgMagenta(" Saving file! Please wait ..."))
		l.CreateFile(Name)
		l.WriteLines(str2, Name)
		fmt.Printf("\033[2K")
		fmt.Printf("\033[A")
		fmt.Println(au.BgGreen(" File Saved!                  "))
	} else {
		fmt.Println(au.BgRed(" No file saved! "))
	}

}

func Menu() {
	l.HomeMessage()
	i := Infos()
	fmt.Println(au.Bold("══════════════════════════════════════════════════════════════════════════════"))
	fmt.Println(au.BgCyan(au.Bold(" Infos : ")))
	fmt.Println(au.Bold(" Number of pastes : "), au.Green(i.pastes))
	fmt.Println(au.Bold(" Number of pages  : "), au.Brown(i.pages))
	fmt.Println(au.Bold(" Number of days   : "), au.Blue(i.days))
	fmt.Println(au.Bold(" Last day         : "), au.Magenta(i.date))
	fmt.Println(au.Bold("══════════════════════════════════════════════════════════════════════════════\n"))
	fmt.Println(au.Bold("══════════════════════════════════════════════════════════════════════════════"))
	fmt.Println(au.BgBlue(au.Bold(" Menu Settings : ")))
	fmt.Println("══════════════════════════════════════════════════════════════════════════════")

	var c Config

	var input string
	var input2 string

	var link []string
	for {
		fmt.Print(au.Brown(au.Bold(" Days[1] / Pages[2] : ")))
		fmt.Scanln(&input)
		if input == "1" {
			fmt.Print(au.Green(au.Bold(" Number days to check : ")))
			fmt.Scanln(&input2)
			v, err := strconv.Atoi(input2)
			if err != nil {
				fmt.Println(au.Red("\nError! Unreconized value : "), input2, "\n")
			} else {
				fmt.Println(au.BgMagenta("\n Grab link for after! Please wait ..."))
				fmt.Printf("\033[2K")
				fmt.Printf("\033[A")
				link = l.RemoveDuplicatesUnordered(Infos().Daysgrab(v))
				fmt.Println(au.Black(au.BgGray(au.Bold(" Number of link used after for graber : "))), au.Bold(len(link)))
				break
			}
		} else if input == "2" {
			fmt.Print(au.Green(au.Bold(" Number pages to check :  ")))
			fmt.Scanln(&input2)
			v, err := strconv.Atoi(input2)
			if err != nil {
				fmt.Println(au.Red("\nError! Unreconized value : "), input2, "\n")
			} else {
				fmt.Println(au.BgMagenta("\n Grab link for after! Please wait ..."))
				fmt.Printf("\033[2K")
				fmt.Printf("\033[A")
				link = l.RemoveDuplicatesUnordered(Infos().Pagegrab(v))
				fmt.Println(au.Black(au.BgGray(au.Bold(" Number of link used after for graber : "))), au.Bold(len(link)))
				break
			}
		} else {
			fmt.Println(au.Red("\nError! Unreconized value : "), input, "\n")
		}
	}
	c.lnkList = link
	var threadInput string
	for {
		fmt.Print("\n Threads [Max Recomended : 5] : ")
		fmt.Scanln(&threadInput)
		v, err := strconv.Atoi(threadInput)
		if err != nil {
			fmt.Println(au.Red("\nError! Unreconized value : "), threadInput, "\n")
		} else {
			c.threads = v
			break
		}
	}
	var tOutInput string
	for {
		fmt.Print(" Timeout in second [Recomended : 15] : ")
		fmt.Scanln(&tOutInput)
		v, err := strconv.Atoi(tOutInput)
		if err != nil {
			fmt.Println(au.Red("\nError! Unreconized value : "), tOutInput, "\n")
		} else {
			c.timeout = v
			break
		}
	}
	var selectGrabInp string
	fmt.Println(au.BgGreen(au.Bold("\n Graber Selector : ")))
	fmt.Println(" 0 - [IP:Port]")
	fmt.Println(" 1 - [Email]")
	fmt.Println(" 2 - [USER:PASS]")
	fmt.Println(" 3 - [EMAIL:PASS]")
	fmt.Println(" 4 - [Numeric Combo]")
	fmt.Println(" 5 - [URL Combo]")
	fmt.Println(" 6 - [URL Email Combo]")
	fmt.Println(" 7 - [Custom]")

	for {
		fmt.Print("\n Select graber : ")
		fmt.Scanln(&selectGrabInp)
		if selectGrabInp == "0" {
			c.regex = SelectGrabber(0, "")
			break
		} else if selectGrabInp == "1" {
			c.regex = SelectGrabber(1, "")
			break
		} else if selectGrabInp == "2" {
			c.regex = SelectGrabber(2, "")
			break
		} else if selectGrabInp == "3" {
			c.regex = SelectGrabber(3, "")
			break
		} else if selectGrabInp == "4" {
			c.regex = SelectGrabber(4, "")
			break
		} else if selectGrabInp == "5" {
			c.regex = SelectGrabber(5, "")
			break
		} else if selectGrabInp == "6" {
			c.regex = SelectGrabber(6, "")
			break
		} else if selectGrabInp == "7" {
			var custom string
			fmt.Print("\n Custom Regex : ")
			fmt.Scanln(&custom)
			c.regex = SelectGrabber(7, custom)
			break
		} else {
			fmt.Println(au.BgRed("\nError! Unreconized value : "), selectGrabInp)
		}
	}
	c.verbose = true
	fmt.Println("══════════════════════════════════════════════════════════════════════════════")
	fmt.Println(au.Bold("══════════════════════════════════════════════════════════════════════════════"))
	l.Pause(false)
	fmt.Println("")
	c.LaunchG(link, *i)
}
