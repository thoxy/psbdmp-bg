package psbdmp

import (
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"regexp"
	"runtime"
	"strconv"
	"time"

	l "gitlab.com/thoxy/psbdmp-bg/libs"
	"gopkg.in/cheggaaa/pb.v1"
)

//list the pastes from the daily pages
func (m *MaxP) Daysgrab(nmbr int) []string {
	var s []string
	max := m.days
	needed := max - nmbr

	for i := max; i > needed; i-- {
		var grabbed string
		grabbed = grabstr("http://psbdmp.ws/daily/"+strconv.Itoa(i), 15)
		r, _ := regexp.Compile("<a\\shref='/[\\w]{0,}'>([\\w]{0,})<")
		for _, t := range r.FindAllStringSubmatch(grabbed, -1) {
			s = append(s, "http://psbdmp.ws/api/dump/get/"+t[1])
		}
	}
	return l.RemoveDuplicatesUnordered(s)
}

//list the pastes from the dump pages
func (m *MaxP) Pagegrab(nmbr int) []string {
	var s []string
	max := m.pages

	for i := 1; i <= nmbr; i++ {
		if i > max {
			break
		}
		var grabbed string
		grabbed = grabstr("http://psbdmp.ws/dumps/"+strconv.Itoa(i), 15)
		r, _ := regexp.Compile("<a\\shref='/[\\w]{0,}'>([\\w]{0,})<")
		for _, t := range r.FindAllStringSubmatch(grabbed, -1) {
			s = append(s, "http://psbdmp.ws/api/dump/get/"+t[1])
		}
	}
	return l.RemoveDuplicatesUnordered(s)
}

/*
	0- ipport        string = "\\b(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\:\\d{1,4})\\b"                                                    //Looks for and saves pattern of type: 166.78.1.37:80
	1- email         string = "(\\b[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}\\b)"                                                    //Looks for and saves pattern of type: admin@bruteit.net
	2- userpass      string = "((?:[a-zA-Z0-9]{5,}):(?:[a-zA-Z0-9]{5,}))"                                                                       //Looks for and saves pattern of type: User:Pass
	3- emailpass     string = "([a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}:(?:[a-zA-Z0-9]{5,}))"                                      //Looks for and saves pattern of type: Email:Pass
	4- numericcombo  string = "(\\b[0-9]{5,}:[0-9]{5,})"                                                                                        //Looks for and saves pattern of type: NumericUser:NumericPass
	5- urlcombo      string = "([hHtTpPsS]{4,5}://[a-zA-Z0-9]{5,}:[a-zA-Z0-9]{5,}@[a-zA-Z0-9._%+-/]+)"                                          //Saves: http(s)://User:Pass@SomeSite.com
	6- urlemailcombo string = "([hHtTpPsS]{4,5}://[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}:(?:[a-zA-Z0-9]{5,})@[a-zA-Z0-9._%+-/]+)" //Saves: http(s)://Email:Pass@SomeSite.com
*/
func SelectGrabber(t int, str string) string {
	var s string
	if t == 0 {
		s = ipport
	} else if t == 1 {
		s = email
	} else if t == 2 {
		s = userpass
	} else if t == 3 {
		s = emailpass
	} else if t == 4 {
		s = numericcombo
	} else if t == 5 {
		s = urlcombo
	} else if t == 6 {
		s = urlemailcombo
	} else if t == 7 && str != "" {
		s = str
	}
	return s
}

func (c Config) grab(ch chan<- []string, jobs <-chan int) {
	var lst []string

	for j := range jobs {
		r, _ := regexp.Compile(c.regex)

		for _, t := range r.FindAllStringSubmatch(grabstr(c.lnkList[j], c.timeout), -1) {
			lst = append(lst, t[1])
		}
		ch <- lst
	}
}

func (c Config) StartWork() []string {
	var listResult []string
	runtime.GOMAXPROCS(4)
	length := len(c.lnkList)
	ch := make(chan []string, length)
	jobs := make(chan int, length)

	for w := 0; w < c.threads; w++ {
		go c.grab(ch, jobs)
	}
	for j := 0; j <= length; j++ {
		if j != length {
			jobs <- j
		} else {
			break
		}
	}
	close(jobs)

	var bar *pb.ProgressBar
	count := length

	bar = pb.New(count)
	bar.Format("║▒█ ║")
	bar.SetWidth(77)
	bar.SetMaxWidth(77)
	bar.ShowPercent = true
	bar.ShowBar = true
	bar.ShowCounters = true
	bar.ShowTimeLeft = true
	bar.ShowSpeed = true
	bar.ShowBar = true
	bar.Start()

	for i := 0; i < length; i++ {
		if i != length {
			cc := <-ch
			if len(cc) != 0 {
				listResult = append(listResult, cc...)
			}
			bar.Increment()
		}
	}
	bar.FinishPrint("\nFinished!")
	return listResult
}

// Get wesite source and return as string
func grabstr(URL string, tOut int) string {
	https := &tls.Config{InsecureSkipVerify: true}
	httpTransport := &http.Transport{TLSClientConfig: https}
	httpClient := &http.Client{Transport: httpTransport, Timeout: time.Second * time.Duration(15)}
	req, _ := http.NewRequest("GET", URL, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0")
	resp, _ := httpClient.Do(req)
	defer resp.Body.Close()
	b, _ := ioutil.ReadAll(resp.Body)
	return string(b)
}
