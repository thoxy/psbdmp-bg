package psbdmp

// max pages
type MaxP struct {
	pastes int
	pages  int
	days   int
	date   string
}

type Config struct {
	threads int      //Number of threads
	timeout int      //Timeout
	regex   string   //Regex used for grabber
	verbose bool     //verbose mode
	lnkList []string //list of link used to check regex
}

type Result []string

const (
	ipport        string = "\\b(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\:\\d{1,4})\\b"                                                    //Looks for and saves pattern of type: 166.78.1.37:80
	email         string = "(\\b[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}\\b)"                                                    //Looks for and saves pattern of type: admin@bruteit.net
	userpass      string = "((?:[a-zA-Z0-9]{5,}):(?:[a-zA-Z0-9]{5,}))"                                                                       //Looks for and saves pattern of type: User:Pass
	emailpass     string = "([a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}:(?:[a-zA-Z0-9]{5,}))"                                      //Looks for and saves pattern of type: Email:Pass
	numericcombo  string = "(\\b[0-9]{5,}:[0-9]{5,})"                                                                                        //Looks for and saves pattern of type: NumericUser:NumericPass
	urlcombo      string = "([hHtTpPsS]{4,5}://[a-zA-Z0-9]{5,}:[a-zA-Z0-9]{5,}@[a-zA-Z0-9._%+-/]+)"                                          //Saves: http(s)://User:Pass@SomeSite.com
	urlemailcombo string = "([hHtTpPsS]{4,5}://[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}:(?:[a-zA-Z0-9]{5,})@[a-zA-Z0-9._%+-/]+)" //Saves: http(s)://Email:Pass@SomeSite.com
)
